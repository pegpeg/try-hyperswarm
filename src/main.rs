use async_std::task;
use futures_lite::{AsyncReadExt, AsyncWriteExt, StreamExt};
use hyperswarm::{Config, Hyperswarm, HyperswarmStream, TopicConfig};
use blake2_rfc::blake2b::blake2b;
use std::io;
use std::convert::TryInto;

#[async_std::main]
async fn main() -> io::Result<()> {
    let config = Config { bootstrap: Some(vec!("185.206.144.57:39613".parse().unwrap())), ephemeral: true };

    let mut swarm = Hyperswarm::bind(config).await?;

    let topic = blake2b(32, &[], b"the topic").as_bytes().try_into().unwrap();

    swarm.configure(topic, TopicConfig::announce_and_lookup());
    println!("Joining topic {}", hex::encode(topic));

    while let Some(stream) = swarm.next().await {
        println!("Connection!");
        task::spawn(on_connection(stream?));
    }

    Ok(())
}

// A HyperswarmStream is AsyncRead + AsyncWrite, so you can use it just
// like a TcpStream. Here, we'll send an initial message and then keep
// reading from the stream until it is closed by the remote.
async fn on_connection(mut stream: HyperswarmStream) -> io::Result<()> {
    stream.write_all(b"hello there").await?;
    let mut buf = vec![0u8; 64];
    loop {
        match stream.read(&mut buf).await {
            Ok(0) => return Ok(()),
            Err(e) => return Err(e),
            Ok(n) => eprintln!("received: {}", std::str::from_utf8(&buf[..n]).unwrap()),
        }
    }
}
